const { join, dirname } = require("path");
const { fileURLtoPath } = require("url");
const JSONdb = require("simple-json-db");
const fs = require("fs");



class LogFile {
    constructor(logPath){
        let folderPath = join(logPath, "/log");
        let filePath = join(folderPath, "/log.json");
        if(!fs.existsSync(folderPath))
            fs.mkdirSync(folderPath, {recursive: true});
        
        try{
            this.db = new JSONdb(filePath);
            if(!this.db.has('quizResults'))
                this.db.set('quizResults', []);
            
        }catch(ex){
            console.log("Exception!");
            console.log(ex);
        }
    }
    addToLog = async (quizDetails, userName, userPin) => {
        let savedQuizzes = this.db.get("quizResults");

        let itemIndex = savedQuizzes.indexOf(savedQuizzes.find((ele) => ele.quizParticipant == userName && ele.participantPin == userPin));
        
        if(itemIndex == -1){
            savedQuizzes.push ({
                quizParticipant: userName, 
                participantPin: userPin,
                quizzes: [
                    {
                        timeLimit: quizDetails.timeLimit, 
                        dateCompleted: Date.now(), 
                        gradedQuizDetails: {
                            questions: quizDetails.questions, 
                            correctAnswers: quizDetails.correctAnswers,
                            selectedAnswers: quizDetails.selectedAnswers
                        }
                    }
                ]
            });
        }else{
            savedQuizzes[itemIndex].quizzes.push({
              timeLimit: quizDetails.timeLimit,
              dateCompleted: Date.now(),
              gradedQuizDetails: {
                questions: quizDetails.questions,
                correctAnswers: quizDetails.correctAnswers,
                selectedAnswers: quizDetails.selectedAnswers,
              },
            });
        }

        await this.db.set("quizResults", savedQuizzes);
        return savedQuizzes;

    }
    getPreviousQuizzes = (userName, userPin) => {
        // TODO - get all of the users from  the file...
        let savedQuizzes = this.db.get("quizResults");
        
        
        if(userName == "admin" && userPin == "1337")
            return savedQuizzes;

        let itemIndex = savedQuizzes.indexOf(savedQuizzes.find((ele) => ele.quizParticipant == userName && ele.participantPin == userPin));

        if(itemIndex == -1) return -1;

        return [savedQuizzes[itemIndex]];
    }
}

exports.LogFile = LogFile;