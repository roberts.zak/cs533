const { join, dirname } = require("path");
const { fileURLToPath } = require("url");
const JSONdb = require("simple-json-db");
const fs = require('fs');



module.exports = class DbAdapater {
  constructor(appData) {
    let folderPath = join(appData, "/cs533quizApplication");
    let filePath = join(folderPath, "/db.json");
    if(!fs.existsSync(folderPath))
      fs.mkdirSync(folderPath, {recursive: true});

    try{
      this.db = new JSONdb(filePath);
      if(!this.db.has('quizes'))
        this.db.set('quizes', []);
        
      }catch(ex){
        console.log("Exception!");
        console.log(ex);
    }
    
  }
  getQuizes = () => {
    return this.db.get('quizes');
  };
  addQuiz = async (newQuiz) => {
    let quizes = this.db.get('quizes');
    quizes.push(newQuiz);
    await this.db.set('quizes', quizes);
    return quizes;
  };
  deleteQuiz = async (index) => {
    let quizes = this.db.get('quizes');
    quizes.splice(index, 1);
    await this.db.set('quizes', quizes);
    return quizes; 
  }
}