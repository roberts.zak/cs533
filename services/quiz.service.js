// import fs from 'fs';
// import { once } from 'events';
// import readline from 'readline';
const fs = require('fs'),
      { once } = require('events'),
      readline = require('readline');

const parseQuizFile = (filePath) => {
    // TODO - some sort of verification that this quiz is a correct pattern
    return new Promise(async (res, err) => {
        let filePathArray = filePath.split("\\");
        let name = filePathArray[filePathArray.length - 1];
        let quiz = {
            name: name,
            questions: [],
        };
        try{
            const rl = readline.createInterface({
                input: fs.createReadStream(filePath),
                crlfDelay: Infinity,
            });
            let currentItems = "q";
            let theQuestion = "";
            let answers = [];
            rl.on("line", (line) => {
                if (line.startsWith("*") || line.trim() === "") return;
                if (line.toLowerCase().startsWith("@q")) {
                currentItems = "q";
                return;
                }
                if (line.toLowerCase().startsWith("@a")) {
                currentItems = "a";
                return;
                }
                if (line.toLowerCase().startsWith("@e")) currentItems = "e";

                if (currentItems == "q") theQuestion += line + " ";

                if (currentItems == "a") answers.push(line);

                if (currentItems == "e") {
                // deal with all the collected items.
                let theAnswer = answers[0];
                answers.splice(0, 1);
                let item = {
                    question: theQuestion,
                    answer: theAnswer,
                    availableAnswers: [...answers],
                };
                quiz.questions.push(item);
                theQuestion = "";
                answers = [];
                }
            });

            await once(rl, "close");
            // return quiz;
            res(quiz);

        }catch(ex){
            err(ex);
        };
    });
};
class Quiz {
  constructor() {
    this.quiz = "";
  }
  setupQuiz = (
    selectedQuiz,
    numberOfQuestions,
    allQuizes,
    timeLimit,
    participant = "",
    participantPin = ""
  ) => {
    this.quiz = allQuizes[selectedQuiz];
    this.totalQuestions = numberOfQuestions;
    this.totalTime = timeLimit;
    this.participant = participant;
    this.participantPin = participantPin;
    this._generateRandomQuestions(numberOfQuestions);
  };
  getParticipant = () => {
    return this.participant
  }
  getParticipantPin = () => {
      return this.participantPin;
  }
  getAllQuestions = () => {
    return this.questions;
  };
  getQuizDetails = () => {
    return {
      questions: this.questions,
      timeLimit: this.totalTime,
      participant: this.participant,
    };
  };
  getNumberOfQuestions = () => {
    return this.totalQuestions;
  };
  getQuestion = (index) => {
    try {
      return this.questions[index];
    } catch (ex) {
      return -1;
    }
  };
  updateQuizProgress = (questionIndex, selectedAnswer) => {
    this.quizProgress[questionIndex] = selectedAnswer;
  };
  getGradedDetails = () => {
    return {
      timeLimit: this.totalTime,
      questions: this.questions,
      correctAnswers: this.correctAnswers,
      selectedAnswers: this.selectedAnswers,
    };
  };
  gradeQuiz = (selectedAnswers) => {
    selectedAnswers = selectedAnswers || this.quizProgress;
    let correctAnswers = 0;
    selectedAnswers.forEach((answer, index) => {
      if (answer + 1 == this.questions[index].answer) correctAnswers++;
    });
    this.correctAnswers = correctAnswers;
    this.selectedAnswers = selectedAnswers;

    // So now from here, we need to fire off the logging of this graded quiz.
  };
  _generateRandomQuestions = (numOfQuestions) => {
    let possibleQuestions = [...this.quiz.questions];
    let selectedQuestions = [];
    for (let i = 0; i != numOfQuestions; i++) {
      let possibleIndex = Math.floor(Math.random() * possibleQuestions.length);
      let actualIndex = this.quiz.questions.indexOf(
        possibleQuestions[possibleIndex]
      );
      selectedQuestions.push(this.quiz.questions[actualIndex]);

      possibleQuestions.splice(possibleIndex, 1);
    }
    this.questions = selectedQuestions;
    this.quizProgress = Array.from(selectedQuestions, (item) => -1);
  };
}

exports.Quiz = Quiz;
exports.parseQuizFile = parseQuizFile;