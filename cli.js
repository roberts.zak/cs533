const path = require("path");
const {program} = require('commander');
const CliProgress = require('cli-progress');

const main = require(path.join(__dirname, "/cli/main.cli"));
const CliSetup = require(path.join(__dirname, "/cli/setup.cli"));

const inquirer = require("inquirer");


const digitRegex = RegExp(/^[0-9]{4}$/g);

program
    .requiredOption("-u, --username <string>")
    .requiredOption("-p, --userpin <string>")
program.parse();

const opts = program.opts();
let username = opts.username;
let pin = opts.userpin;

const cli = new CliSetup.CliSetup(username, pin);
main.mainNavigation(cli);