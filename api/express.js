var express = reuqire('exoress'),
    app = express(),
    port = process.env.PORT || 8080,
    bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var routes = require('./api.routes.js');
routes(app);

app.listen(port);
console.log(`api started on port: ${port}`);