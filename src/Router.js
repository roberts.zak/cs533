import React, {useEffect, useState} from 'react';
import App from './App.js';
import {HashRouter, BrowserRouter} from 'react-router-dom';

const electron = window.require("electron");
const ipcRenderer = electron.ipcRenderer;

const Router = () => {
    const  [isDev, setIsDev] = useState(false);
    
    ipcRenderer.on("returnEnv", (event, resp) => {
        setIsDev(resp);
    })

    useEffect(() => {
        ipcRenderer.send("getEnv", {});
    }, []);


    if(isDev){
        return (
            <BrowserRouter>
                <App />
            </BrowserRouter>
        );
    }
    return (
        <HashRouter>
            <App />
        </HashRouter>
    );
}

export default Router;