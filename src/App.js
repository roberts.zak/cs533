import { useState, useEffect} from 'react';
import { useNavigate } from "react-router-dom";

// import axios from 'axios';
import {
  Routes,
  Route,
  useLocation
} from "react-router-dom";
  
import {
    Layout,
    Quiz,
    QuizSelect, 
    Score, 
    QuizDelete, 
    QuizHistory
  } from './components';
    
const electron = window.require('electron');
const ipcRenderer = electron.ipcRenderer;

const App = () =>  {
  let navigate = useNavigate();
  let location = useLocation();
  const [deleteQuiz, setDeleteQuiz] = useState(false);
  const [quizQuestions, setQuizQuestions] = useState([]);

  useEffect(() => {
    ipcRenderer.on("deleteQuiz", (event, message) => {
      setDeleteQuiz(true);
    });
    ipcRenderer.on("openHistory", (event, message) => {
      navigate("history");
    })
  }, [])

  useEffect(() => {
    if(!deleteQuiz) return; 
    if(location.pathname != '/'){
      ipcRenderer.send("openErrorDialog", {
        title: "Unable to Delete Quiz",
        content: "You are unable to delete a quiz in your current view.",
      });
      setDeleteQuiz(false);
      return;
    }
    navigate("delete");
  }, [deleteQuiz]);


  // {/* <Route path="/quiz" element={<Quiz />} /> */}
  // {/* <Route path="/" element={<QuizSelect />} /> */}
  return (
    // TODO - the history.push is not triggering the URL to go to the new page, look into how I should now be handling this.
    <div className="App">
        <Layout>
            <Routes>
              <Route path="/" element={<QuizSelect />} />
              <Route path="/quiz" element={<Quiz />} />
              <Route path="/score" element={<Score />} />
              <Route path="/delete" element={<QuizDelete />} />
              <Route path="/history" element={<QuizHistory />} />
            </Routes>
          {/* <Route path="/" element={<QuizDelete />} /> */}
        </Layout>
    </div>
  );
}

export default App;
