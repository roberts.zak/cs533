import React, { useState, useEffect } from 'react';
import './QuizSelect.scss';
import Slider from 'react-input-slider';
import { useNavigate } from 'react-router-dom';
import ReactTooltip from 'react-tooltip';


const electron = window.require("electron");
const ipcRenderer = electron.ipcRenderer;

const QuizSelect = (props) => {
  let navigate = useNavigate();
  
  const [availableQuizes, setAvailableQuizes] = useState(); 
  const [questions, setQuestions] = useState([]);
  const [selectedQuiz, setSelectedQuiz] = useState();
  const [maxX, setMaxX ] = useState(0);
  const [state, setState] = useState({ x: 4, y: 10 });
  const [errors, setErrors] = useState([]);
  const [quizTakerName, setQuizTakerName] = useState("");
  const [participantPin, setParticipantPin] = useState(""); 
  const [timeLimit, setTimeLimit] = useState("");

  
 
  useEffect(() => {
    ipcRenderer.send("getQuizes", {}); 
    ipcRenderer.on("returnQuizes", (event, args) => {
      setQuestions(args);
    });
  
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    
    if (!quizTakerName || quizTakerName == ""){
      setErrors(["You must provide your name before you are able to continue"]);
      return;
    }
    if (!participantPin || !RegExp(/^[0-9]{4}$/g)){ 
      setErrors(["You must provide a valid 4-digit pin before you are able to continue"]);
    }
      if (!selectedQuiz || selectedQuiz == "null") {
        setErrors(["You must select a quiz before you can continue"]);
        return;
      }

    if(!timeLimit || timeLimit == ""){
      setErrors(["You must provide a time limit before you can continue"]);
      return;
    }


    setErrors([]);
    ipcRenderer.send('selectedQuiz', {
      participant: quizTakerName,
      participantPin: participantPin,
      quiz: selectedQuiz, 
      numOfQuestions: state.x, 
      timeLimit: timeLimit
    });
    navigate('quiz');
  }
  const quizChange = (event) => {
     // TODO - update total number of questions
    let index = event.target.value;
    if(index == "null"){
      setMaxX(0);
      setSelectedQuiz("null");
      return;
    }
    let question = questions[index];
    setMaxX(question.questions.length);
    setSelectedQuiz(index);
  }
  const reloadWindow = (e) => {
    e.preventDefault();
    ipcRenderer.send("reloadWindow", {});
  }

  const onChangeName = (e) => {
    setQuizTakerName(e.target.value);
  };
  const onChangeTimeLimit = (e) => {
    if(!RegExp(/^[0-9]*$/g).test(e.target.value)) {
      setTimeLimit((prevState) => (prevState));
      return;
    }else{
      setTimeLimit(e.target.value);
    }
  }
  const onChangePin = (e) => {
    if(!RegExp(/^[0-9]{0,4}$/g).test(e.target.value)){
      setParticipantPin((prevState) => (prevState));
      return;
    }
    setParticipantPin(e.target.value);
  }
  
  return (
    <div className="QuizSelect">
      <h1>Select a Quiz</h1>
      {errors.length > 0 && (
        <>
          {errors.map((error, index) => (
            <p className="error" key={index}>
              {error}
            </p>
          ))}
        </>
      )}
      <form onSubmit={handleSubmit}>
        <p style={{ fontSize: ".9rem" }}>
          Select a Quiz Below, or add a new quiz through the "Manage Quizzes"
          menu option along the top.
        </p>
        <label htmlFor="quizTakerName">Your Name</label>
        <br />
        <input
          type="text"
          name="quizTakerName"
          id="quizTakerName"
          value={quizTakerName}
          onChange={(e) => onChangeName(e)}
        />
        <br />
        <br />
        <label htmlFor="participantPin">Your Pin</label>
        <br />
        <p className="pinDetails">
          Your pin is a unique 4 digit number and is required. You may use the
          combination of your name and your pin number to access your past quiz
          attempts, which can be accessed through "File &gt; View Past Quizzes"{" "}
        </p>
        <input
          type="text"
          name="particpantPin"
          id="participantPin"
          value={participantPin}
          onChange={(e) => onChangePin(e)}
        />
        <br />
        <br />

        <label htmlFor="quizSelect">Available Quizzes</label>
        <br />
        <select name="quizSelect" id="quizSelect" onChange={quizChange}>
          <option value="null"> - </option>
          {questions && (
            <>
              {questions.map((q, i) => (
                <option key={i} value={i}>
                  {q.name}
                </option>
              ))}
            </>
          )}
        </select>
        <button
          className="reload"
          onClick={(e) => reloadWindow(e)}
          data-tip={"Refresh Quiz List"}
        >
          <svg version="1.1" width="16" height="16" viewBox="0 0 16 16">
            <path d="M13.901 2.599c-1.463-1.597-3.565-2.599-5.901-2.599-4.418 0-8 3.582-8 8h1.5c0-3.59 2.91-6.5 6.5-6.5 1.922 0 3.649 0.835 4.839 2.161l-2.339 2.339h5.5v-5.5l-2.099 2.099z"></path>
            <path d="M14.5 8c0 3.59-2.91 6.5-6.5 6.5-1.922 0-3.649-0.835-4.839-2.161l2.339-2.339h-5.5v5.5l2.099-2.099c1.463 1.597 3.565 2.599 5.901 2.599 4.418 0 8-3.582 8-8h-1.5z"></path>
          </svg>
        </button>
        <ReactTooltip />
        <br />
        <label>How many questions would you like in the quiz?</label>
        <br />
        {maxX > 0 ? state.x : <em>Select a Quiz</em>}
        <br />
        <Slider
          axis="x"
          xstep={1}
          x={state.x}
          xmin={1}
          xmax={maxX}
          onChange={({ x }) => setState((state) => ({ ...state, x }))}
          disabled={maxX < 0}
        />
        <br />
        <br />
        <label htmlFor="timeLimit">Enter a Time Limit in Seconds</label>
        <br />
        <input
          type="text"
          name="timeLimit"
          id="timeLimit"
          value={timeLimit}
          onChange={(e) => onChangeTimeLimit(e)}
        />
        <br />
        <br />
        <button>Begin</button>
      </form>
    </div>
  );
};

export default QuizSelect;
