import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './QuizHistory.scss';

const electron = window.require('electron');
const ipcRenderer = electron.ipcRenderer;

const QuizHistory = () => {
    let navigate = useNavigate();

    const [userName, setUserName] = useState(""); 
    const [userPin, setUserPin] = useState(""); 
    const [errors, setErrors] = useState("")

    const [quizHistory, setQuizHistory] = useState([]); 

    const updateUserName = (e) => {
        setUserName(e.target.value);
    }
    const updateUserPin = (e) => {
        if (!RegExp(/^[0-9]{0,4}$/g).test(e.target.value)) {
            setUserPin((prevState) => prevState);
            return;
        }
        setUserPin(e.target.value);
    }
    const fetchUserHistory = (e) => {
        e.preventDefault();
        if(userName == "" || !RegExp(/^[0-9]{4}$/g).test(userPin)){
            setErrors("Please Provide a Valid Username and User Pin");
            return;
        }
        ipcRenderer.send("getUserHistory", {
            user: userName, 
            pin: userPin
        });

        ipcRenderer.on("retrievingUserHistory", (event, args) => {
            if(args == -1){
                setErrors("No results for User Name/Pin combination, please try again");
                return;
            }
           setQuizHistory(args);
        });
    }
    const backHome = (e) => {
        e.preventDefault(e);
        navigate('/');
    }
     const generateAnswer = (answer, theQuestions, selectedAnswers, i, j) => {
        let classes = [];
        let sa = selectedAnswers[i];
        let correctAnswer = theQuestions[i].answer;
        if(sa == j) classes.push("selected");
        if(j + 1 == correctAnswer){
            classes.push("answer");
        } 
            
        return <li className={classes.join(" ")}>{answer}</li>;
     };

    return (
      <div className="History">
        <div className="userDetails">
          <p className="errrors">{errors}</p>
          <form onSubmit={(e) => fetchUserHistory(e)}>
            <div className="group userDetails">
              <div className="fltleft text-align-left">
                <label htmlFor="userName">Username</label>
                <br />
                <input
                  type="text"
                  name="userName"
                  id="userName"
                  onChange={updateUserName}
                  value={userName}
                />
              </div>
              <div className="fltleft text-align-left">
                <label htmlFor="userPin">User Pin</label>
                <br />
                <input
                  type="text"
                  name="userPin"
                  id="userPin"
                  onChange={updateUserPin}
                  value={userPin}
                />
              </div>
            </div>
            <button>Get History</button>&nbsp;&nbsp;
            <button onClick={(e) => backHome(e)}>Back</button>
          </form>
        </div>
        <div className="userHistory">
          {quizHistory.map((item, index) => (
            <div key={index} className="userGroup">
              <div className="quizTaker text-align-left">
                <p>
                  <strong>Quiz Takers Name:</strong> {item.quizParticipant}
                </p>
                {item.quizzes.map((quiz, jindex) => (
                  <div key={`${index}${jindex}`} className="quiz">
                    <div className="group quizDetails">
                      <div className="fltleft">
                        <p>
                          <strong>Questions Correct:</strong>{" "}
                          {quiz.gradedQuizDetails.correctAnswers}/
                          {quiz.gradedQuizDetails.questions.length}
                          <br />
                          <strong>Score:</strong>{" "}
                          {Math.round(
                            (quiz.gradedQuizDetails.correctAnswers /
                              quiz.gradedQuizDetails.questions.length) *
                              100
                          )}
                          %
                        </p>
                      </div>
                      <div className="fltright">
                        <p>
                          <strong>Date Taken:</strong>{" "}
                          {new Date(quiz.dateCompleted).toDateString()}
                          <br />
                          <strong>Time Limit:</strong> {quiz.timeLimit / 1000}{" "}
                          seconds
                        </p>
                      </div>
                    </div>
                    <div className="quizQuestions">
                      <ol className="questions">
                        {quiz.gradedQuizDetails.questions.map(
                          (question, questionIndex) => (
                            <li
                              key={`${index}${jindex}${questionIndex}`}
                                className={
                                  question.answer == quiz.gradedQuizDetails.selectedAnswers[questionIndex] + 1
                                    ? "correct"
                                    : "incorrect"
                                }
                            >
                              <strong>{question.question}</strong>
                              <ol type="A" className="answers">
                                {question.availableAnswers.map((answer, j) => (
                                  <React.Fragment
                                    key={`${index}${jindex}${questionIndex}${j}`}
                                  >
                                    {generateAnswer(
                                      answer,
                                      quiz.gradedQuizDetails.questions,
                                      quiz.gradedQuizDetails.selectedAnswers,
                                      questionIndex,
                                      j
                                    )}
                                  </React.Fragment>
                                ))}
                              </ol>
                            </li>
                          )
                        )}
                      </ol>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          ))}
        </div>
      </div>
    );
}

export default QuizHistory; 