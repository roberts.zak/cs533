import React, { useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom';
import './Quiz.scss';
import ReactTooltip from 'react-tooltip';
import Countdown from "react-countdown";

import QuestionsDoc from "../../demo_questions.json"; 

const electron = window.require("electron");
const ipcRenderer = electron.ipcRenderer;

const Quiz = (props) => {
  let navigate = useNavigate(); 
  const [questions, setQuestions] = useState(); 
  // const [quiz, setQuiz] = useState();
  const [timeLimit, setTimeLimit] = useState(); 
  const [quizProgress, setQuizProgress] = useState([]);
  const [timeWarning, setTimeWarning] = useState(false);
  const [activeQuestion, setActiveQuestion] = useState({
    number: 1,
    question: "",
    selectedAnswer: -1, 
    answers: []
  })

  ipcRenderer.on("continueWithQuizGrade", (event, args) => {
    // TODO - maybe just redirect here? 
    // fireGradeQuiz();
    navigate("/score");
  });

  useEffect(() => {
    // if (props.questions.length == 0) return;
    ipcRenderer.on("returnQuizQuestions", (event, details) => {
      // setQuizQuestions(questions);
      let questions = details.questions, 
          participant = details.participant;

      console.log(details.timeLimit);
      setTimeLimit(details.timeLimit * 1000);
      setQuestions(questions);
      setQuizProgress(Array.from(questions, (item) => -1));
      setActiveQuestion((prevState) => ({
        ...prevState,
        question: questions[0].question,
        answers: questions[0].availableAnswers,
      }));
    });


  }, [props])



  const fireGradeQuiz = () => {
    if(quizProgress.length == 0)
      return;
    ipcRenderer.send("gradeQuiz", quizProgress);
    navigate("/score");
  }

  const changeQuestion = (e, questionNumber) =>{
    setActiveQuestion((prevState) => ({
      ...prevState, 
      number: questionNumber,
      question: questions[questionNumber - 1].question,
      answers: questions[questionNumber - 1].availableAnswers, 
      selectedAnswer: quizProgress[questionNumber - 1]
    }));

  }
  const selectAnswer = (event, value) => {
    let currentIndex = activeQuestion.number - 1;
    let progress = [...quizProgress];
    progress[currentIndex] = value;
    setQuizProgress(progress);
    setActiveQuestion((prevState) => ({
      ...prevState,
      selectedAnswer: progress[currentIndex],
    }));
  }

  const handleQuizSubmit = () => {
    if(quizProgress.includes(-1)){
      ipcRenderer.send("openNoticeDialog", {title: "Incomplete Quiz", message: "You're attempting to finish the quiz while you still have unanswered questions. Are you sure you would like to continue and have the quiz graded? This action cannot be undone.", currentProgress: quizProgress});
      return; 
    }
    fireGradeQuiz();
  };
  const onTick = (obj) => {
    if(obj.total > 300000) return;
    setTimeWarning(true);
  }
  const quizTimeout = () => {
    alert("You have ran out of time, your quiz will now be graded.");
    fireGradeQuiz();
  }
  const goBack = () => {
    navigate("/")
  }
  if(!questions || !timeLimit){
    return (
      <div className="Quiz">
        Loading Quiz...<br />
        <button onClick={goBack}>Go back</button>
      </div>
    )
  }

  return (
    <div className="Quiz">
      <div className={`timer${timeWarning ? " warning" : ""}`}>
        Time Remaining: <Countdown zeroPadTime={2} zeroPadDays={0} onTick={(tick) => onTick(tick)} date={Date.now() + timeLimit} onComplete={quizTimeout} />

      </div>
      <h2 className="question">{activeQuestion.question}</h2>
      {activeQuestion.answers.length > 0 && (
        <ol type="A" className="answers">
          {activeQuestion.answers.map((answer, i) => (
            <li key={i}>
              <input 
                type="radio" 
                name={`answer`} 
                value={i} 
                checked={activeQuestion.selectedAnswer === i}
                onChange={(e) => selectAnswer(e, i)}
              />
              {answer}
            </li>
          ))}
        </ol>
      )}
      <div className="controls group">
        <button
          className="fltleft"
          onClick={(e) => changeQuestion(e, (activeQuestion.number-1))}
          disabled={activeQuestion.number === 1}
        >
          Previous
        </button>
        <button
          className="fltright"
          onClick={(e) => changeQuestion(e, (activeQuestion.number+1))}
          disabled={activeQuestion.number === questions?.length}
        >
          Continue
        </button>
      </div>
      {quizProgress && quizProgress.length <= 250 && (
        <div className="testProgress">
          <ul>
            {quizProgress.map((status, i) => (
              <li key={i}>
                <button
                  className={`${
                    i + 1 === activeQuestion.number ? "active" : ""
                  } ${status >= 0 ? "complete" : ""}`}
                  data-for={`nav-${i}`}
                  data-tooltip={`Question ${i + 1} | ${
                    status > 1 ? "Completed" : "Incomplete"
                  }`}
                  onClick={(e) => changeQuestion(e, i+1)}
                />
                <ReactTooltip id={`nav-${i}`} />
              </li>
            ))}
          </ul>
        </div>
      )}
      <div className="submit">
        <button className="flt-right" onClick={handleQuizSubmit}>Submit Quiz</button>
      </div>
    </div>
  );
};

export default Quiz;
