import React, { useState, useEffect } from 'react';
import './QuizDelete.scss';
import Slider from 'react-input-slider';
import { useNavigate } from 'react-router-dom';
import ReactTooltip from 'react-tooltip';

const electron = window.require("electron");
const ipcRenderer = electron.ipcRenderer;

const QuizDelete = (props) => {
  let navigate = useNavigate();
  const [quizes, setQuizes] = useState();
  const [selectedQuiz, setSelectedQuiz] = useState();
  const [quizDeleted, setQuizDeleted] = useState(false);

  ipcRenderer.on("quizDeleted", (event, args) => {
    setQuizDeleted(true);
  });
  ipcRenderer.on("returnQuizes", (event, args) => {
    setQuizes(args);
  });

  useEffect(() => {
    if(!quizDeleted) return;
    fetchQuizes();
    setQuizDeleted(false);

  }, [quizDeleted])
  useEffect(() => {
    fetchQuizes();
  }, [])
  const fetchQuizes = () => {
    ipcRenderer.send("getQuizes", {});
  }
  const quizChange = (event) => {
    let index = event.target.value;
    setSelectedQuiz(index);

  }
  const reloadWindow = (e) => {
    e.preventDefault();
    ipcRenderer.send("reloadWindow", {});
  };

  const handleSubmit = () => {
    ipcRenderer.send("confirmDelete", selectedQuiz);
    // Prompt with a confirmation to make sure that you want to delete. 
  }
  const returnToQuizes = (e) => {
    e.preventDefault();
    navigate("/");
  }
  return (
    <div className="QuizDelete">
      <h1>Delete a Quiz</h1>
      <form onSubmit={handleSubmit}>
        <p>Select the quiz to delete from the list below</p>
        <select name="quizSelect" id="quizSelect" onChange={quizChange}>
          <option value="null"> - </option>
          {quizes &&
            quizes.map((q, i) => (
              <option key={i} value={i}>
                {q.name}
              </option>
            ))}
        </select>
        <button
          className="reload"
          onClick={(e) => reloadWindow(e)}
          data-tip={"Refresh Quiz List"}
        >
          <svg version="1.1" width="16" height="16" viewBox="0 0 16 16">
            <path d="M13.901 2.599c-1.463-1.597-3.565-2.599-5.901-2.599-4.418 0-8 3.582-8 8h1.5c0-3.59 2.91-6.5 6.5-6.5 1.922 0 3.649 0.835 4.839 2.161l-2.339 2.339h5.5v-5.5l-2.099 2.099z"></path>
            <path d="M14.5 8c0 3.59-2.91 6.5-6.5 6.5-1.922 0-3.649-0.835-4.839-2.161l2.339-2.339h-5.5v5.5l2.099-2.099c1.463 1.597 3.565 2.599 5.901 2.599 4.418 0 8-3.582 8-8h-1.5z"></path>
          </svg>
        </button>
        <ReactTooltip />
        <br /><br />
        <button>Delete Quiz</button><br /><br />
        <button onClick={(e) => returnToQuizes(e)}>Back to Quiz Selection</button>
      </form>
    </div>
  );
};

export default QuizDelete;
