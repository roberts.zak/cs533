export { default as QuizHistory } from './QuizHistory/QuizHistory';
export {default as QuizDelete} from './QuizDelete/QuizDelete';
export {default as QuizSelect} from './QuizSelect/QuizSelect';
export {default as Layout} from './Layout/Layout';
export {default as Score} from './Score/Score';
export {default as Quiz} from './Quiz/Quiz';