import React, {useState, useEffect} from 'react';
import { useNavigate } from "react-router-dom";
import './Score.scss';

const electron = window.require("electron");
const ipcRenderer = electron.ipcRenderer;

const Score = () => {
  let navigate = useNavigate();

  const [score, setScore] = useState();
  const [correctQuestions, setCorrectQuestions] = useState();
  const [selectedAnswer, setSelectedAnswer] = useState();
  const [totalQuestions, setTotalQuestions] = useState();
  const [theQuestions, setQuestions] = useState(); 

  useEffect(() => {
    ipcRenderer.send("getQuizScore", {}); 
    ipcRenderer.on("recieveQuizScore", (event, args) => {
      setQuestions(args.questions);
      setSelectedAnswer(args.selectedAnswers);
      setTotalQuestions(args.questions.length);
      setCorrectQuestions(args.correctAnswers);
      setScore(Math.round((args.correctAnswers/args.questions.length) * 100));
    });
  }, [])
  const generateAnswer = (answer, i, j) => {
      let classes = [];
      let sa = selectedAnswer[i];
      let correctAnswer = theQuestions[i].answer;
      if(sa == j) classes.push("selected");

      if((j + 1) == correctAnswer) classes.push('answer');
      return (
        <li className={classes.join(" ")}>
          {answer}
        </li>
      )
  }
  const handleClick = (e) => {
    e.preventDefault();
    ipcRenderer.send("resetQuiz", {});
    navigate('/');
  }
return (
  <div className="Score">
    <h2>Quiz Score: {score}%</h2>
    <p>
      Questions Correct: {correctQuestions}/{totalQuestions}
    </p>
    <div className="questionsBox">
      <ol className="questions">
        {theQuestions &&
          selectedAnswer &&
          theQuestions.map((question, i) => (
            <li
              key={i}
              className={
                question.answer == selectedAnswer[i] + 1
                  ? "correct"
                  : "incorrect"
              }
            >
              <strong>{question.question}</strong>
              <ol type="A" className="answers">
                {question.availableAnswers.map((answer, j) => (
                  <React.Fragment key={`${i}${j}`}>
                    {generateAnswer(answer, i, j)}
                  </React.Fragment>
                ))}
              </ol>
            </li>
          ))}
      </ol>
    </div>
    <button onClick={(e) => handleClick(e)}>Done</button>
  </div>
);
};
export default Score;
