# ASSIGNMENT No. 1
<p style="text-align: center">CS 533--Spring 2022<br/>Jim Mooney<br >
Lane Dept. of Computer Science & Electrical Engineering<br>
West Virginia University<br>
DATE DUE: Thursday, Feb. 10, 2022<br>
TOTAL POINTS: 10</p>

## OVERVIEW
Your first assignment is to develop a simple program to conduct an interactive quiz. The program should read a set of questions and associated answers from a sequential text file. Each question consists of a series of up to ten lines, and has up to ten associated answers of one line each. The complete file may contain up to 10 000 questions. The format of a question file is described and illustrated in detail in the sample file sample.q. *Your program must accept any input file which follows this description!*

Ater reading the file, the program should select questions at random and present them one at a time on the display screen. Each possible answer should also be displayed. The user should then be prompted to select an answer. When a valid answer is selected, the program should report if
the answer is right or wrong, and proceed to the next question. Reasonable attention should be given to handling erroneous input and to presenting a good user interface.

The questions should continue until all questions have been asked, or until a specified limit has been reached. No question should be asked twice.

At the end of the session, the program should report the total number of questions asked, the number of correct answers, the percent correct, and the elapsed time for the quiz.

The question file name and the number of questions to ask should be able to be specified as options for each run of the program.

Note that the questions to be asked for each session should be randomly selected from all questions available. If your program is run twice with the same parameters, it should not ask the same questions. If five questions are to be asked out of twenty, any of the twenty should be equally likely to appear.

The Project Notes page contains some hints to help you organize your program. If you run into difficulty, your instructor may be willing to provide some examples to get you started.

Feel free to discuss the project with other students, but please submit your own work. Projects that are unreasonably similar in appearance, behavior, or logical structure may not be accepted!

## SYSTEM INFORMATION
All students should prepare this program to run correctly on an "IBM PC" running Microsoft Windows 7 or a later version, using your chosen language. Note that program files may be prepared using your favorite editor or development environment in Windows, or in another environment such as Linux or Macintosh. Programs prepared on a different system may be transferred to Windows for compiling and execution.

This assignment may require you to learn certain details of the Windows environment interfaces, such as system calls, language details, or library functions. Your instructor can provide some guidance with these problems as they arise. Questions will be accepted by email at any time.

Project support files and useful system information will be available on the Project Notes page. Skeleton versions of the program in one or more selected languages are also available on the project notes page.

 I will install your program on my computer, and test it with my question files. Be sure you provide full installation instructions. Do not expect me, the user, to compile your source code. Do not expect my computer to have any special tools or environments available. Do not make assumptions about the organization of my system or file hierarchy. For example, the name
of my main drive is not necessarily "C:" !

You should also provide execution instructions distinct from installation instructions. The user should not have to install the program each time he/she runs it! Your program may run in a pure "text" mode, with all input and output as lines of text. Of course, this is not the way a typical Windows program works. Extra points may be available for a more "Windows-like" implementation.

## RECORD KEEPING
Even though this is a small program, it is important to develop it using good software engineering techniques. Furthermore, you are asked to keep a log of your development activities; you will be asked for reports based on this log. As a minimum, you should keep track of the
following:
1. Approximate time in hours you spend on each of the following activities:
    1. Learning the System
    2. Program Design
    3. Implementation
    4. Test and Debug
    5. Documentation
    6. Other
2. Summary of any problems or difficulties you encounter during the development.

## WHAT TO SUBMIT

1. Executable files for your program (email attachments or downloads)
2. Source files for your program (via eCampus)
3. Complete instructions on program installation and execution (via eCampus)
4. A completed report using the form provided (MS Word format, via eCampus).
You should provide a default question file of your own devising. Interesting questions are encouraged! I will test your program by executing both your file and my own test files. Your program should be runnable by any user logged in to the computer on which it is installed.


## GRADING
This assignment will be graded on the following scale (Note the emphasis on good instructions!):
| params   | points |
| -- | -------|
| Execution | 6 pts |
| Instructions | 2 pt |
| Source files | 1 pt |
| Report | 1 pts | 
| TOTAL | 10 pts |
<!-- <Br><Br><Br><Br><Br><Br>
________
# React Specific Notes
# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify) -->
