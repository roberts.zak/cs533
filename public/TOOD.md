- Update react to pull all data through the ipcRenderer
- finish node backend stuff
- make sure that we can parse and save the selectedFile as a database file
- save that databaseFile off in the app-data 
- - app.getPath('appData') will default this out for any os that we're using GUI wise, ubuntu we'll just have to save it locally
- - remove all of the express stuff, instead just setup a file at the root where I can pass in all of the data, and call it all in through the electron. 

