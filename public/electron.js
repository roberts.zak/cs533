let DEV_MODE = true;

// Pull in all of the dependencies
const { app, BrowserWindow, ipcMain, dialog, Menu } = require("electron");
const path = require("path");

const isMac = process.platform === 'darwin';
const quizService = require(path.join(__dirname, "../services/quiz.service"));
const DbAdapter = require(path.join(__dirname, "../services/db.service"));
const LogFile = require(path.join(__dirname, "../services/log.service"));

// Setup variables
const DATABASE = new DbAdapter(app.getPath('appData'));
const LOG = new LogFile.LogFile(path.join(__dirname, ".."));
let QUIZ = new quizService.Quiz();


// Create the window icon
const createWindow = () => {
    const win = new BrowserWindow({
        title: "WVU CS 533 - Assignment 1",
        width: 800,
        height: 600,
        webPreferences: {
            enableRemoteModule: true,
            nodeIntegration: true,
            contextIsolation: false,
            // preload: './preload.js'
        },
    });
    // Create the top navigation
    const menu = Menu.buildFromTemplate([
      ...(isMac
        ? [
            {
              label: app.name,
            },
          ]
        : []),
      {
        label: "File",
        submenu: [
          isMac ? { role: "close" } : { role: "quit" },
          {
            label: "View Past Quizzes", 
            click: async() => {
              win.webContents.send("openHistory");
            }
          }
        ],
      },
      {
        label: "Manage Quizzes",
        submenu: [
          {
            label: "Import a New Quiz",
            click: async () => {
              openFileDialog();
            },
          },
          {
            label: "Remove a Quiz",
            click: async () => {
              win.webContents.send("deleteQuiz");
            },
          },
        ],
      },
      {
        label: "View",
        submenu: [
          { role: 'reload' },
          { role: 'forceReload' },
          { role: 'toggleDevTools' },
          { type: 'separator' },
          { role: 'resetZoom' },
          { role: 'zoomIn' },
          { role: 'zoomOut' },
          { type: 'separator' },
          { role: 'togglefullscreen' }
        ]
      }
    ]);

    Menu.setApplicationMenu(menu);
    ipcMain.on("reloadWindow", (event, args) => {
      win.reload(); 
    });
    if(DEV_MODE){  
      win.loadURL("http://localhost:3000");
    }else{
      win.loadURL(`file://${path.join(__dirname, "../build/index.html")}`);
    }
};

// Create the window
app.whenReady().then(() => {
    createWindow();
    app.on("activate", () => {
        if (BrowserWindow.getAllWindows().length === 0) createWindow();
    });
});
// quit app if window is closed, except for on mac
app.on("window-all-closed", () => {
  if (!isMac) app.quit();
});

ipcMain.on("getEnv", (event, args) => {
  event.reply("returnEnv", DEV_MODE);
})

const gradeTheQuiz = (args) => {
  QUIZ.gradeQuiz(args);
  LOG.addToLog(
    QUIZ.getGradedDetails(),
    QUIZ.getParticipant(),
    QUIZ.getParticipantPin()
  ); 
}

//participant
//participantPin
//quiz
//numOfQuestions
//timeLimit

// Listens for the selectedQuiz event, and gets the questions for that quiz
ipcMain.on("selectedQuiz", (event, args) => {
  QUIZ.setupQuiz(
    args.quiz,
    args.numOfQuestions,
    DATABASE.getQuizes(),
    parseInt(args.timeLimit),
    args.participant,
    args.participantPin
  );
  let quizDetails = QUIZ.getQuizDetails();
  event.reply('returnQuizQuestions', quizDetails);
})
// Listens for the getQuizes request and returns all of the available quizes
ipcMain.on("getQuizes", (event, args) => {
  let quizes = DATABASE.getQuizes();
  event.reply("returnQuizes", quizes)
});
// Listens for the "openFileDialog event and fires off the openFileDialog function"
ipcMain.on("openFileDialog", (event, args) => {
  if (!args) return;
  openFileDialog(event);
});
// If there is an error, open the error dialog and display the message
ipcMain.on("openErrorDialog", (event, args) => {
  if(args)
    dialog
      .showErrorBox(
        args.title, 
        args.content
      )
});
// Open the file explorer to upload a new quiz
ipcMain.on("openNoticeDialog", (event, args) => {
  dialog.showMessageBox(
      {
        title: args.title,
        message: args.message, 
        type: "warning", 
        buttons: ["Submit and Grade the Quiz", "Cancel"]
      }
    ).then((res) => {
      if(res.response === 0){
        gradeTheQuiz(args.currentProgress);
        event.reply("continueWithQuizGrade", {});
      }
    }).catch((err) => {
      console.log(err);
    });
})
// Grade the quiz
ipcMain.on("gradeQuiz", (event, args) => {
  // Create the log instance 
  gradeTheQuiz(args);
});
// Get the score details of the quiz
ipcMain.on("getQuizScore", (event, args) => {
  let details = QUIZ.getGradedDetails()
  event.reply("recieveQuizScore", details); 
});
// Reset the quiz back to a new quiz
ipcMain.on("resetQuiz", (event, args) => {
  QUIZ = new quizService.Quiz();
})
// Dialog to confirm quiz deletion 
ipcMain.on("confirmDelete", (event, args) => {
  dialog
    .showMessageBox({
      title: "Confirm Quiz Deletion",
      message: "Are you sure you would like to delete the selected quiz?",
      type: "warning",
      buttons: ["Delete Quiz", "Cancel"],
    })
    .then((resp) => {
      if (resp.response == 0) {
        DATABASE.deleteQuiz(args);
        event.reply("quizDeleted");
      }else{
          // TODO - not delete the quiz
        // console.log("we should not delete the quiz!");
      }
    });
});
ipcMain.on("getUserHistory", (event, args) => {
  let userHistory = LOG.getPreviousQuizzes(args.user, args.pin);
  event.reply("retrievingUserHistory", userHistory);
});
// Opens the file dialog to select a file
const openFileDialog = (event = null) => {
  dialog
    .showOpenDialog({
      title: "Select a Quiz File",
      defaultPath: app.getPath("home"),
      properties: ["openFile"],
      filters: [{ name: "Quiz Files", extensions: ["q", "txt"] }],
    })
    .then(({ canceled, filePaths }) => {
      quizService
        .parseQuizFile(filePaths[0])
        .then((resp) => {
          DATABASE.addQuiz(resp).then((res) => {
            event.reply("returnQuizes", res);
          }).catch((err) => {
            console.log(err);
          });
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log("inside of the fileFetch err");
      console.log(err);
    });
}

const openPastQuizzes = (event = null) => {
  event.reply("openHistory");
  console.log("open the past quizzes for this, will need to provide name");
}


