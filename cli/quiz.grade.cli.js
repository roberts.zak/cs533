const path = require('path');
const chalk = require('chalk');
const main = require(path.join(__dirname, "main.cli"));


const gradedQuiz = (cli) => {
    let quiz = cli.getQuiz();
    let rl = cli.getReadLine();
    cli.clearTimer();

    let gradedQuizDetails = quiz.getGradedDetails();
    let answeredCorrectly = gradedQuizDetails.correctAnswers
    let theQuestion = `\n\n\n${chalk.bgYellow.black.bold(
      `Your Score: ${answeredCorrectly}/${
        gradedQuizDetails.questions.length
      } | ${Math.round(
        (answeredCorrectly / gradedQuizDetails.questions.length) * 100
      )}%`
    )}\n\n`;

    gradedQuizDetails.questions.map((q, i) => {
        
        let answer = q.answer;
        let availableAnswers = q.availableAnswers;
        let chosenAnswer = gradedQuizDetails.selectedAnswers[i];
        
        // if(typeof gradedQuizDetails.selectedAnswers == 'undefined') {
          // chosenAnswer = -5;
        // }
        // else{ 
          // chosenAnswer = gradedQuizDetails.selectedAnswers[i];
        // }
        
        if(answer == (chosenAnswer + 1)){
            theQuestion += `${chalk.bold.green(q.question)}\n`;
        }else{
            theQuestion += `${chalk.bold.red(q.question)}\n`;
        }

        availableAnswers.map((a, j) => {
            if(j == (chosenAnswer)){
                if(j == (answer - 1))
                    theQuestion += `${chalk.underline.green(a)}\n`;
                else
                    theQuestion += `${chalk.underline.red(a)}\n`;
                return false; 
            }

            if(j == (answer - 1)){
                theQuestion += `${chalk.green(a)}\n`;
                return false;
            }
            theQuestion += `${a}\n`;        
        });
        theQuestion += `\n\n`;
        
    })
    
    theQuestion += `${chalk.bgYellow.black.bold(
      `Your Score: ${answeredCorrectly}/${
        gradedQuizDetails.questions.length
      } | ${Math.round(
        (answeredCorrectly / gradedQuizDetails.questions.length) * 100
      )}%`
    )}\n\n`;

    theQuestion += `(Any key + enter/return to return to the main menu, or "Q" to quit)`;

    rl.question(theQuestion, (line) => {
        if (line.toLowerCase() == "q") {
            return rl.close();
        }
        return main.mainNavigation(cli);
    });
}
exports.gradedQuiz = gradedQuiz;  