const path = require("path");
const chalk = require('chalk');

const quizSelect = require(path.join(__dirname, "quiz.select.cli"));
const quizImport = require(path.join(__dirname, "import.cli"));
const quizDelete = require(path.join(__dirname, "quiz.delete.cli"));
const quizHistory = require(path.join(__dirname, "quiz.history.cli"));

const mainNavigation = (cli) => {
  let rl = cli.getReadLine();
  let user = cli.getUsername();
  let pin = cli.getPin();

  if(user == "admin" && pin == "1337"){
    rl.question(
      "Please Choose an option:\n" +
        "1) View all users past quizzes\n" + 
        "2) Exit\n", 
        (line) => {
          switch(line) {
            case "1":
              return quizHistory.adminQuizHistory(cli);
            case "2": 
              return rl.close();
            default: 
              console.log(
                chalk.red.bold("\n\nNo such option. Please enter another: ")
              );
              mainNavigation(cli);
          }
        }
    )
  }
  else{
    rl.question(
      "Please Choose an option:\n" +
        "1) Select a Quiz\n" +
        "2) Import a New Quiz\n" +
        "3) View Past Quiz Attempts\n" + 
        "4) Delete a Quiz\n" +
        "5) Exit\n",

      (line) => {
        switch (line) {
          case "1":
            quizSelect.selectQuiz(cli);
            break;
          case "2":
            quizImport.importQuiz(cli);
            break;
          case "3": 
            quizHistory.quizHistory(cli);
            // View past quizzes;
            break;
          case "4":
            quizDelete.deleteQuiz(cli);
            break;
          case "5":
            return rl.close();
          default:
            console.log(
              chalk.red.bold("\n\nNo such option. Please enter another: ")
            );
            mainNavigation(cli); //Calling this function again to ask new question
        }
      }
    );
  }
};

exports.mainNavigation = mainNavigation;
