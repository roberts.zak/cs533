const path = require('path');
const chalk = require('chalk');
const digitRe = new RegExp("^[0-9]+$");
const main  = require(path.join(__dirname, "main.cli"));

const quizHistory = (cli, quizTakersIndex = 0) => {
    let pin = cli.getPin();
    let log = cli.getLog();
    let rl = cli.getReadLine();
    let username = cli.getUsername();
    // if(username == "admin" && pin == "1337"){
    //     return adminQuizHistory(cli);
    // }
    let theQuestion = `\n\n\nSelect a Previous Quiz that you would like to review.\n`;
    let theQuizHistory = log.getPreviousQuizzes(username, pin);

    theQuizHistory[quizTakersIndex].quizzes.forEach((item, index) => {
      theQuestion += `${index + 1}) Quiz Taken: ${new Date(
        item.dateCompleted
      )} | ${
        item.gradedQuizDetails.questions.length
      } question(s) | Score ${Math.ceil(
        (item.gradedQuizDetails.correctAnswers /
          item.gradedQuizDetails.questions.length) *
          100
      )}%\n`;
    });
    theQuestion += `Or press B to go back, or Q to Quit\n`;

    rl.question(theQuestion, (line) => {
        if (line.toLowerCase() == "q") return rl.close();
        if (line.toLowerCase() == "b") {
          console.log("\n\n\n");
          return main.mainNavigation(cli);
        }

        if(digitRe.test(parseInt(line))){
            let index = parseInt(line);
            if(index > 0 && index <= theQuizHistory[quizTakersIndex].quizzes.length)
              return usersQuizHistory(cli, quizTakersIndex, line - 1);

        }

        console.log(chalk.red.bold("\n\nInvalid Selection, please try again"));
        return quizHistory(cli, quizTakersIndex);
    }) 
}

const usersQuizHistory = (cli, userIndex, selectedQuiz) => {
  let rl = cli.getReadLine();
  let log = cli.getLog();
  let username = cli.getUsername();
  let pin = cli.getPin();
  let theQuizHistory = log.getPreviousQuizzes(username, pin);

  let gradedQuizDetails = theQuizHistory[userIndex].quizzes[selectedQuiz].gradedQuizDetails;

  let answeredCorrectly = gradedQuizDetails.correctAnswers;
  let theQuestion = `\n\n\n${chalk.bgYellow.black.bold(
    `Your Score: ${answeredCorrectly}/${
      gradedQuizDetails.questions.length
    } | ${Math.round(
      (answeredCorrectly / gradedQuizDetails.questions.length) * 100
    )}%`
  )}\n\n`;

  gradedQuizDetails.questions.map((q, i) => {
    let answer = q.answer;
    let availableAnswers = q.availableAnswers;
    let chosenAnswer = gradedQuizDetails.selectedAnswers[i];

    // if(typeof gradedQuizDetails.selectedAnswers == 'undefined') {
    // chosenAnswer = -5;
    // }
    // else{
    // chosenAnswer = gradedQuizDetails.selectedAnswers[i];
    // }

    if (answer == chosenAnswer + 1) {
      theQuestion += `${chalk.bold.green(q.question)}\n`;
    } else {
      theQuestion += `${chalk.bold.red(q.question)}\n`;
    }

    availableAnswers.map((a, j) => {
      if (j == chosenAnswer) {
        if (j == answer - 1) theQuestion += `${chalk.underline.green(a)}\n`;
        else theQuestion += `${chalk.underline.red(a)}\n`;
        return false;
      }

      if (j == answer - 1) {
        theQuestion += `${chalk.green(a)}\n`;
        return false;
      }
      theQuestion += `${a}\n`;
    });
    theQuestion += `\n\n`;
  });

  theQuestion += `${chalk.bgYellow.black.bold(
    `Your Score: ${answeredCorrectly}/${
      gradedQuizDetails.questions.length
    } | ${Math.round(
      (answeredCorrectly / gradedQuizDetails.questions.length) * 100
    )}%`
  )}\n\n`;

  theQuestion += `(Any key + enter/return to return to history selection, "B" to return to the main menu, or "Q" to quit)`;

  rl.question(theQuestion, (line) => {
    if(line.toLowerCase() == "b"){
      return main.mainNavigation(cli);
    }
    if (line.toLowerCase() == "q") {
      return rl.close();
    }
    if(username == "admin" && pin == "1337")
      return adminQuizHistory(cli);

    return quizHistory(cli); 
  });
}


const adminQuizHistory = (cli) => {
  // OK, so first thing we need to do is get all of the users. 
  let rl = cli.getReadLine();
  let pin = cli.getPin();
  let log = cli.getLog();
  let username = cli.getUsername();
  if(username != "admin" || pin != "1337")
    return quizHistory(cli);

  let theQuizHistory = log.getPreviousQuizzes(username, pin);

  console.log(quizHistory);
  let theQuestion = "\n\nSelect the user whos quiz history you would like to view\n";

  theQuizHistory.forEach((item, index) => {
    theQuestion += `${index + 1}) ${item.quizParticipant}\n`
  })
  theQuestion += `Or press B to go back, or Q to Quit\n`;

  rl.question(theQuestion, (line) => {
    if (line.toLowerCase() == "q") return rl.close();
    if (line.toLowerCase() == "b") {
      console.log("\n\n\n");
      return main.mainNavigation(cli);
    }

    if (digitRe.test(parseInt(line))) {
      let index = parseInt(line);
      if (index > 0 && index <= theQuizHistory.length)
        return quizHistory(cli, line - 1);
    }

    console.log(chalk.red.bold("\n\nInvalid selection, please try again"));
    return adminQuizHistory(cli);
  });
}

exports.quizHistory = quizHistory;
exports.adminQuizHistory = adminQuizHistory;