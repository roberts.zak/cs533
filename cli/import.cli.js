const path = require("path");
const chalk = require("chalk");
const main = require(path.join(__dirname, "main.cli"));
const fs = require("fs");

const urlRegEx = new RegExp("^(.+)/([^/]+).[(txt)|(q)]$");

const importQuiz = (cli) => {
    let rl = cli.getReadLine(); 
    let database = cli.getDatabase();
    let quiz = cli.getQuizService();
    
    rl.question(
        "Please provide the full path to your your .q or .txt file, or press \"B\" to return back to the main menu or \"Q\" to quit:\n",
        (line) => {
            if(line.toLowerCase() == "b"){
                return main.mainNavigation(cli);
            }
            if(line.toLowerCase() == "q"){
                rl.close();
            }
            if (!urlRegEx.test(line)) {
              console.log(
                chalk.bold.red("Please provide a valid txt or q file")
              );
              return importQuiz(cli);
            }
            if(!fs.existsSync(line)){
                console.log(chalk.bold.red("File not found, please enter try again"));
                return importQuiz(cli);
            }

            quiz
              .parseQuizFile(line)
              .then((resp) => {
                database.addQuiz(resp)
                  .then((res) => {
                    console.log(chalk.bold.green("Quiz Added Successfully"));
                    return main.mainNavigation(cli);
                  })
                  .catch((err) => {
                    console.log(chalk.bold.red(err));
                    importQuiz(cli);
                  });
              })
              .catch((err) => {
                console.log(chalk.bold.red(err));
                importQuiz(cli);
              });
        }
    )
}

exports.importQuiz = importQuiz;