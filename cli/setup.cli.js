const path = require("path");
const readline = require("readline");

const quizService = require(path.join(__dirname, "../services/quiz.service"));
const DbAdapter = require(path.join(__dirname, "../services/db.service"));
const LogFile = require(path.join(__dirname, "../services/log.service"));


const APPDATA =
  process.env.APPDATA ||
  (process.platform == "darwin"
    ? process.env.HOME + "/Library/Preferences"
    : process.env.HOME + "/.local/share");

class CliSetup {
  constructor(username, pin, time) {
    this.rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    this.log = new LogFile.LogFile(path.join(__dirname, ".."));
    this.database = new DbAdapter(APPDATA);
    this.quiz = new quizService.Quiz();
    this.username = username;
    console.log(this.username);
    this.timelimit = time;
    this.pin = pin;
    console.log(this.pin);
  }
  initializeSetup() {
    // TODO -  we want all of the CLi calls coming from here...
  }
  getUsername(){
    return this.username;
  }
  getPin(){ 
    return this.pin;  
  }
  getTimeLimit() { 
    return this.timelimit;
  }
  setTimeLimit(timelimit) {
    this.timelimit = timelimit;
  }
  setTimer(timer) { 
    this.timer = timer;
  }
  getTimeRemaining() {
    return Math.ceil(
      (this.timer._idleStart + this.timer._idleTimeout) / 1000 - process.uptime()
    );
  }
  clearTimer() {
    clearTimeout(this.timer);
  }
  getLog() {
    return this.log;
  }
  getQuiz() {
    return this.quiz;
  }
  getQuizService() {
    return quizService;
  }
  getDatabase() {
    return this.database;
  }
  getReadLine() {
    return this.rl;
  }
}

exports.CliSetup = CliSetup;