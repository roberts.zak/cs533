const path = require('path');
const chalk = require("chalk");
const digitRe = new RegExp("^[0-9]+$");
const main = require(path.join(__dirname, "main.cli"))

const deleteQuiz = (cli) => {
    let database = cli.getDatabase();
    let quiz = cli.getQuiz(); 
    let rl = cli.getReadLine();

    let question = `\n\nselect a quiz from the list below to delete, or type "Q" to Quit Application or "B" to return to the main menu`;

    let quizes = database.getQuizes(); 
    if(quizes.length == 0) {
        question = `You currently don't have any quizzes saved, type "B" to return to the main menu and import a new quiz\n`;
    }else{
        question += "\n";
        quizes.map((quiz, index) => {
            question += `${index + 1}) ${quiz.name}\n`;
        });
    }

    rl.question(question, (line) => {
        if(line.toLowerCase() == "q"){
            return rl.close();
        }
        if(line.toLowerCase() == "b"){
            console.log("\n\n\n");
            return main.mainNavigation(cli);
        }
        if (quizes.length == 0 && (line.toLowerCase() != "b")) {
            console.log(
                chalk.red.bold(
                    '\n\nYou must select "B" to return to the main menu and try again'
                )
            );
        }
        if(digitRe.test(parseInt(line))){
            let digit = parseInt(line);
            if(digit < 0 || digit > quizes.length){
                console.log(
                    chalk.red.bold(
                        `\n\nInvalid Selection, please choose a number between 1 and ${quizes.length} (inclusive)`
                    )
                )
            }
            return confirmQuizDelete(cli, (digit - 1))
        }
        console.log(chalk.bold.red("Invalid Selection, please try again"));
        return deleteQuiz();
    })
}

const confirmQuizDelete = (cli, selectedQuiz) => {
    let database = cli.getDatabase();
    let rl = cli.getReadLine();

    let question = chalk.bold.yellow(`\nSelected Quiz is about to be deleted. Pelase type "CONFIRM" to confirm the deletion, or type "B" to return to the main menu or "Q" to quit.\n`);

    rl.question(question, (line) => {
        if(line.toLowerCase() == "q")
            return rl.close();
        if(line.toLowerCase() == "b"){
            console.log("\n\n\n");
            return main.mainNavigation(cli);
        }
        if(line.toLowerCase() == "confirm"){
            // TODO - delete the selected quiz
            database.deleteQuiz(selectedQuiz);
            console.log(chalk.green.bold("\nQuiz Successfully Deleted"));
            return main.mainNavigation(cli);
        }
        
        console.log(chalk.red.bold("Invalid input provided"));
        return deleteQuiz(cli);
    })

}
exports.deleteQuiz = deleteQuiz;