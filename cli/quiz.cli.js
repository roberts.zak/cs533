const path = require('path');
const chalk = require('chalk');
const digitRe = new RegExp("^[0-9]+$");
const main = require(path.join(__dirname, "main.cli"));
const quizGrade = require(path.join(__dirname, "quiz.grade.cli"));

let ac;
let signal;


const quizCli = (cli, activeQuestionIndex = 0) => {
    ac = new AbortController();
    signal = ac.signal;

    signal.addEventListener("abort", () => {}, { once: true });
    
    let quiz = cli.getQuiz();
    let log = cli.getLog();
    let rl = cli.getReadLine(); 
    let question = quiz.getQuestion(activeQuestionIndex);
    let totalQuestions = quiz.getNumberOfQuestions();

    let timeRemaining = cli.getTimeRemaining();
    let theQuestion = "";
    if (timeRemaining < 11) {
      theQuestion += chalk.bold.red(`Time Remaining: ${timeRemaining} seconds`);
    } else if (timeRemaining < 21) {
      theQuestion += chalk.bold.yellow(`Time Remaining: ${timeRemaining} seconds`);
    } else {
      theQuestion += chalk.bold.green(`Time Remaining: ${timeRemaining} seconds`);
    }
    theQuestion += `\n(${activeQuestionIndex + 1}/${totalQuestions}) ${question.question}\n\n`;

    question.availableAnswers.map((answer, index) => {
        theQuestion += `${index + 1}) ${answer}\n`;
    });

    theQuestion += `\nEnter a the corresponding number to choose your answer, or press "Q" to quit, or "B" to return to the main navigation. ${chalk.bold(`Quiz Progress will not be saved if you quit or return.`)}\nYou may also type "GRADE" to stop quiz progress now and grade the quiz.\n`;

    rl.question(theQuestion, {signal}, (line) => {
        if (line.toLowerCase() == "q") return rl.close();
        if (line.toLowerCase() == "b") {
          console.log("\n\n\n");
          return main.mainNavigation(cli);
        }
        if(line.toLowerCase() == "grade"){
          return runGradeQuiz(quiz, quizGrade, log, cli);
        }
        if(digitRe.test(parseInt(line))){
            let digit = parseInt(line);
            if(digit < 1 || digit > question.availableAnswers.length){
                console.log(
                  chalk.red.bold(
                    `\n\nInvalid Selection, please choose a number between 1 and ${question.availableAnswers.length} (inclusive).`
                  )
                );
                return quiz(cli, activeQuestionIndex);
            }
            // Successfully guessed a question. 
            quiz.updateQuizProgress(activeQuestionIndex, (digit - 1));
            
            if (activeQuestionIndex == (totalQuestions - 1)){
              return runGradeQuiz(quiz, quizGrade, log, cli);
            }
            
            return quizCli(cli, activeQuestionIndex + 1);
        }

        console.log(chalk.red.bold("\n\nInvalid Selection, please try again"));
        return quizCli(cli, activeQuestionIndex);
    });
}


const runGradeQuiz = (quiz, quizGrade, log, cli) => {
  quiz.gradeQuiz();
  log.addToLog(quiz.getGradedDetails(), cli.getUsername(), cli.getPin());
  return quizGrade.gradedQuiz(cli);
}

const cancelQuiz = () => {
  ac.abort();
}

exports.quizCli = quizCli;
exports.cancelQuiz = cancelQuiz;
exports.runGradeQuiz = runGradeQuiz;