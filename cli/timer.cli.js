const path = require('path');
const chalk = require("chalk");
const quizCli = require(path.join(__dirname, "quiz.cli"));
const quizGrade = require(path.join(__dirname, "quiz.grade.cli"));

const setupTimer = (cli) => {
    let timer = cli.getTimeLimit();
    let quiz = cli.getQuiz();
    let log = cli.getLog();

    const testing = new setTimeout(() => {
        quizCli.cancelQuiz();
        console.log(chalk.red.bold("Your Time Limit has elapsed. Your Quiz will now be graded."));
        clearTimeout(testing);
        return quizCli.runGradeQuiz(quiz, quizGrade, log, cli);
        // quiz.gradeQuiz();
        // return quizGrade.gradedQuiz(cli);
        // quiz.gradeQuiz();
        // quizGrade.gradedQuiz(cli);
    }, (timer * 1000));

    cli.setTimer(testing);
    return quizCli.quizCli(cli);

} 
exports.setupTimer = setupTimer;