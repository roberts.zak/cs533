const path = require('path');
const chalk  = require("chalk");
const digitRe = new RegExp("^[0-9]+$");
const main = require(path.join(__dirname, "main.cli"));
const quizCli = require(path.join(__dirname, "quiz.cli"));
const timerCli = require(path.join(__dirname, "timer.cli"));

let selectedQuizIndex = -1;

const selectQuiz = (cli) => {
  let database = cli.getDatabase();
  let quiz = cli.getQuiz();
  let rl = cli.getReadLine();
  let question =
    '\n\nSelect a quiz from the list below, or type "Q" to Quit application or "B" to return to the main menu where you can import a new quiz.\n';

  let quizes = database.getQuizes();

  if (quizes.length == 0) {
    question +=
      'You currently have no quizes saved, please press "B" to return to main menu and import a new quiz\n';
  } else {
    // TOOD - loop through the quizes, pin them on.
    question += "\n"
    quizes.map((quiz, index) => {
      question += `${index + 1}) ${quiz.name}\n`;
    });
  }

  rl.question(question, (line) => {
    if(line.toLowerCase() == "q"){
      return rl.close();
    }
    if(line.toLowerCase() == "b"){
      console.log("\n\n\n");
      return main.mainNavigation(cli);
    }
    if (quizes.length == 0 && (line.toLowerCase() != "b")) {
      console.log(
        chalk.red.bold(
          '\n\nYou must select "B" to return to the main menu and try again'
        )
      );
      return selectQuiz(cli);
    }
     
    if(digitRe.test(parseInt(line))){
      let digit = parseInt(line);
      if (digit < 1 || digit > quizes.length) {
        console.log(
            chalk.red.bold(
                `\n\nInvalid Selection, please choose a number between 1 and ${quizes.length} (inclusive).`
            )
        );
        return selectQuiz(cli);
      }
      selectedQuizIndex = parseInt(line) - 1;
      return numberOfQuestions(cli);
    }
    // Default case
    console.log(chalk.red.bold("\n\nInvalid Selection, please try again!"));
    return selectQuiz(cli);
  });
};

const numberOfQuestions = (cli) => {
  let database = cli.getDatabase(); 
  let quiz = cli.getQuiz();
  let rl = cli.getReadLine();

  let quizes = database.getQuizes();
  let theQuiz = quizes[selectedQuizIndex];

  let question = `\n\nHow many questions would you like to answer? ${theQuiz.name} has a total of ${theQuiz.questions.length}. Enter B to return to Quiz Select, or Q to quit\n`;

  rl.question(question, (line) => {
    if(line.toLowerCase() == "q")
      return rl.close();
    if(line.toLowerCase() == "b"){
      console.log("\n\n\n");
      return selectQuiz(cli);
    }
    if(digitRe.test(parseInt(line))){
      let digit = parseInt(line);
      if(digit < 1 || digit > theQuiz.questions.length){
        console.log(
          chalk.red.bold(
            `\n\nInvalid number of questions, please enter a number between 1 and ${theQuiz.questions.length} (inclusive)`
          )
        );
        return numberOfQuestions(cli);
      }

      quiz.setupQuiz(
        selectedQuizIndex, 
        digit, 
        quizes, 
        cli.getPin(), 
        cli.getPin()
      );
      // return quizCli.quizCli(cli);
      return inputTimeLimit(cli);

    }
    console.log(chalk.red.bold(`Invalid input, please try again`));
    return numberOfQuestions(cli);
  });
} 

const inputTimeLimit = (cli) => {
  let database = cli.getDatabase();
  let quiz = cli.getQuiz();
  let rl = cli.getReadLine();

  let question = `\n\nPlease enter a time limit for taking this quiz in seconds. Enter Q to quit\n`;
  rl.question(question, (line) => {
    if(line.toLowerCase() == "q"){
      rl.close();
    }
    if(!digitRe.test(parseInt(line))){
      console.log(chalk.red.bold(`Please provide the number of seconds.`));
      return inputTimeLimit(cli);
    }
    cli.setTimeLimit(line);
    return timerCli.setupTimer(cli);
  })

}


exports.selectQuiz = selectQuiz;